$("#fileUpload").on('change', function () {
         
    if (typeof (FileReader) != "undefined") {
 
        var image_holder = $("#image-holder");
        image_holder.empty();
 
        var reader = new FileReader();
        reader.onload = function (e) {
            $("<img />", {
                "src": e.target.result,
                "class": "container thumb-image"
            }).appendTo(image_holder);
        }
        image_holder.show();
        reader.readAsDataURL($(this)[0].files[0]);
    } else{
        alert("Este navegador nao suporta FileReader.");
    }
});

function mandatoryNotes(){
    var formvalue = "invoiceAttributesDetailsFORM";
    var queryString = "&EUAM_SELECTED_FORM=" + formvalue;
    var legendURL = "/EUAM/ADRGateway?jadeAction=MANDATORY_NOTES_ACTION_HANDLER";

    validateInput(document.invoiceAttributesDetailsFORM);

    if (checkValidation == "true") {
      submitSpecialBidDetails(document.invoiceAttributesDetailsFORM);

      jQuery.ajax({
        url: "MandatoryNotes.jsp",
        data: queryString,
        success: function(response){
          console.log(response);
        }
      });
    }
    }